module.exports.Token = require('./token.model');
module.exports.User = require('./user.model');
module.exports.Health = require('./health.model');
module.exports.HealthProfile = require('./healthprofile.model');