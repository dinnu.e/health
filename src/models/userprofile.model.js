const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');
const validator = require('validator');


const userProfileSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    mobile:{
      type: String,
      required: true,
      unique: true,
    },
    date_of_birth:{
      type: String,
      required: true,
    },
    daily_step_goal:{
      type: Number,
      required: true,
    },

  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
userProfileSchema.plugin(toJSON);


/**
 * @typedef UserProfile
 */
const UserProfile = mongoose.model('UserProfile', userProfileSchema);

module.exports = UserProfile;
