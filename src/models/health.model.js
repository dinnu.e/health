const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const healthSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    type: {
      type: Object,
      required: true,
    },
    timestamp: Date,
  },
  {
    timeseries: {
      timeField: 'timestamp',
      metaField: 'metadata',
      granularity: 'hours',
    },
  }
);

// add plugin that converts mongoose to json
healthSchema.plugin(toJSON);

/**
 * @typedef Health
 */
const Health = mongoose.model('Health', healthSchema);

module.exports = Health;
