const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const healthProfileSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    averageStepCount: {
      type: String,
      required: true,
    },
    averageSleepTime: {
        type: String,
        required: true,
    },
    averageCaloriesBurned: {
        type: String,
        required: true,
    },
  }
);

// add plugin that converts mongoose to json
healthProfileSchema.plugin(toJSON);

/**
 * @typedef HealthProfile
 */
const HealthProfile = mongoose.model('HealthProfile', healthProfileSchema);

module.exports = HealthProfile;
