const express = require('express');
const validate = require('../../middlewares/validate');
const authValidation = require('../../validations/auth.validation');
const authController = require('../../controllers/auth.controller');
const healthController = require('../../controllers/health.controller');
const auth = require('../../middlewares/auth');

const router = express.Router();

router.get('/', auth(), healthController.getHealthRecord);
router.post('/updatehealth', auth(), healthController.health);
router.delete('/updatehealth', auth(), healthController.deleteHealthRecord);

module.exports = router;
