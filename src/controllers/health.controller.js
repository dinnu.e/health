const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { healthService, emailService } = require('../services');

const health = catchAsync(async (req, res) => {
  const healthParams = await healthService.healthData(req.user.id, req.body);
  const isStepGoalReached = await healthService.verifyStepCountGoal(healthParams)
  if(isStepGoalReached){
    emailService.sendDailyGoalAppreciationEmail(req.user.id)
  }
  const healthProfile = await healthService.updateAverageHealthProfile(req.user.id);
  res.send(healthProfile);
});

const getHealthRecord = catchAsync(async (req, res) => {
  const healthRecords = await healthService.getHealthRecords(req.user.id);
  res.send(healthRecords);
});

const deleteHealthRecord = catchAsync(async (req, res) => {
  await healthService.deleteHealthRecord(req.body.id);
  const healthProfile = await healthService.updateAverageHealthProfile(req.user.id);
  res.send(healthProfile);
});


module.exports = {
  health,
  getHealthRecord,
  deleteHealthRecord
}