const ObjectId = require('mongoose').Types.ObjectId
const userService = require('./user.service');
const { Health, HealthProfile } = require('../models');
const ApiError = require('../utils/ApiError');
const { log } = require('../config/logger');
const httpStatus = require('http-status');
const UserProfile = require('../models/userprofile.model');


/**
 * Post health data
 * @param {string} date
 * @param {string} type
 * @returns {Promise<User>}
 */
const healthData = async (userid, data) => {
  data.user = userid;

  //set date to default UTC

  const clientUtcDate = new Date(data.timestamp);
  const startOfDay = new Date(clientUtcDate.toISOString().slice(0, 10));
  const tomorrow = new Date(startOfDay);
  tomorrow.setDate(tomorrow.getDate() + 1);
  const dateOnly = startOfDay.toISOString().split('T')[0];
  
  const query = {
    user: ObjectId(userid),
    timestamp: {
      $gte: startOfDay,
      $lt: tomorrow
    }
  };

  const update = {
    $set: {
      user: ObjectId(userid),
      timestamp: dateOnly,
      type: data.type
    }
  };

  const options = {
    upsert: true,
    new: true
  };

  try {
    const document = await Health.findOneAndUpdate(query, update, options);
    if (document) {
      return document;
    }
  } catch (error) {
    console.error('Error updating/inserting health record:', error);
    throw error;
  }
};


const getAverageHealthProfile = async (userid) => {

  try{
    return Health.aggregate([
      {
        $match: {
          user:  ObjectId(userid),
        }
      },
      {
        $group: {
          _id:  null,
          averageCaloriesBurned: { $avg: '$type.calories' },
          averageSleepTime: { $avg: '$type.sleep' },
          averageStepCount: { $avg: '$type.stepcount' }          
        }
      }
    ]).then((result) => {
      if (result.length > 0) {
        return {
          averageStepCount : result[0].averageStepCount,
          averageSleepTime : result[0].averageSleepTime,
          averageCaloriesBurned : result[0].averageCaloriesBurned,
        }
      } 
    })
    .catch((error) => {
      console.error('Error calculating average count:', error);
    });

  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Calculating average count failed');
  }
} 


const updateAverageHealthProfile = async (userid) => {
  const healthProfileAverage = await getAverageHealthProfile(userid)
  Object.assign(healthProfileAverage, {user : userid})
  const healthParams =  await HealthProfile.findOneAndUpdate(userid, { $set: healthProfileAverage}, { upsert: true, new: true, user: 0 }).select('-user')
  return healthParams;
};


const verifyStepCountGoal = async (healthparams) => {
  try {
    const userProfile =  await UserProfile.findOne({ "user": healthparams.user });
    return (userProfile.stepcount >= userProfile['daily_step_goal']) ? true : false ;
  } catch (err) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Verify step failed');
  }
}


const getHealthRecords = async (userid) => {
  try {
    return await Health.find({user : userid}, {user : 0});
  } catch (err) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'GET failed');
  }
}


const deleteHealthRecord = async (id) => {
  try {
    const query = { _id: id };
    const { deletedCount } = await Health.deleteOne(query);
    if (deletedCount === 1) {
      console.log('Document deleted successfully');
    } else {
      console.log('Document not found');
    }
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Delete failed');
  }
}
// db.getCollection('health').find({user : ObjectId("64a933adab4e28e392b02697"), timestamp:{$gte: ISODate("2023-07-08T00:00:00Z"), $lt: ISODate("2023-07-08T23:59:59Z")}}, {type:1});

module.exports = {
  healthData,
  getHealthRecords,
  updateAverageHealthProfile,
  verifyStepCountGoal,
  deleteHealthRecord
};